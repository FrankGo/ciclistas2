<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class= "jumbotron">
    <h1><b>Consutas de Selección Ciclistas<b/></h1>
        <h2><i>Ejercicio Nº2</i></h2> <br>
    </div>
    
  <div class="d-flex justify-content-center">
   <div class="row">
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº1</h2>
        <p>Número de ciclistas que hay.</p>
      <?= Html::a('Active Record', ['/site/consulta1a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta1'], ['class' => 'btn btn-default']) ?> <br><br><br>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº2</h2>
        <p>Número de ciclistas que hay del equipo Banesto.</p>
      <?= Html::a('Active Record', ['/site/consulta2a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta2'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº3</h2>
        <p>Edad media de los ciclistas.</p>
            <?= Html::a('Active Record', ['/site/consulta3a'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('DAO', ['/site/consulta3'], ['class' => 'btn btn-default']) ?> <br><br><br>
        </div>
    </div>
      <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº4</h2>
        <p>La edad media de los del equipo Banesto.</p>
            <?= Html::a('Active Record', ['/site/consulta4a'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('DAO', ['/site/consulta4'], ['class' => 'btn btn-default']) ?> <br><br><br>
        </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº5</h2>
        <p>La edad media de los ciclistas por cada equipo.</p>
      <?= Html::a('Active Record', ['/site/consulta5a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta5'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nª6</h2>
        <p>El número de ciclistas por equipo.</p>
      <?= Html::a('Active Record', ['/site/consulta6a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta6'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº7</h2>
        <p>El número total de puertos.</p>
      <?= Html::a('Active Record', ['/site/consulta7a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta7'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
      <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº8</h2>
        <p>El número total de puertos mayores de 1500.</p>
      <?= Html::a('Active Record', ['/site/consulta8a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta8'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
      </div>
     <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº9</h2>
        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas.</p>
      <?= Html::a('Active Record', ['/site/consulta9a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta9'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
     <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº10</h2>
        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.</p>
      <?= Html::a('Active Record', ['/site/consulta10a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta10'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
    <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº11</h2>
        <p>Indicar el número de etapas que ha ganado cada uno de los ciclistas.</p>
      <?= Html::a('Active Record', ['/site/consulta11a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta11'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
    <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº12</h2>
        <p>Indicar el dorsal de los ciclistas que hayan ganado más de una etapa.</p>
      <?= Html::a('Active Record', ['/site/consulta12a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta12'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
    </div>
</div>