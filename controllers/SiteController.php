<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Etapa;
use app\models\Puerto;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


//EJERCICIO 2 DE CONSULTAS
    
    //Consulta Nº1
    
    //Utilizando el método DAO

    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) AS total FROM ciclista;')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS total FROM ciclista;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta Nº1 con DAO",
            "enunciado"=>"Número de ciclistas que hay.",
            "sql"=>'SELECT COUNT(*) As total FROM ciclista;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta1a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->select('COUNT(*) AS total'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta Nº1 con ORM",
            "enunciado"=>"Número de ciclistas que hay.",
            "sql"=>'SELECT COUNT(*) AS total FROM ciclista;',
        ]);
    }
    //Consulta Nº2
    
    //Utilizando el método DAO

    public function actionConsulta2(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) AS total FROM ciclista WHERE nomequipo="Banesto";')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS total FROM ciclista WHERE nomequipo="Banesto";',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta Nº2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto.",
            "sql"=>'SELECT COUNT(*) As total FROM ciclista WHERE nomequipo="Banesto";',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta2a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->select('COUNT(*) AS total')->where('nomequipo="Banesto"'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta Nº2 con ORM",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto.",
            "sql"=>'SELECT total FROM ciclista WHERE nomequipo="Banesto";',
        ]);
    }
    
    //Consulta Nº3
    
    //Utilizando el método DAO

    public function actionConsulta3(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT AVG(edad) AS edad_media FROM ciclista;')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) AS edad_media FROM ciclista;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta Nº3 con DAO",
            "enunciado"=>"Edad media de los ciclistas.",
            "sql"=>'SELECT AVG(edad) AS edad_media FROM ciclista;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta3a(){
             $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->select('AVG(edad) AS edad_media'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta Nº3 con ORM",
            "enunciado"=>"Edad media de los ciclistas.",
            "sql"=>'SELECT AVG(edad) AS edad_media FROM ciclista;',
        ]);
    }
    
    //Consulta Nº4
    
    //Utilizando el método DAO

    public function actionConsulta4(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT AVG(edad) AS edad_media FROM ciclista WHERE nomequipo="Banesto";')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) AS edad_media FROM ciclista WHERE nomequipo="Banesto";',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta Nº4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto.",
            "sql"=>'SELECT AVG(edad) AS edad_media FROM ciclista WHERE nomequipo="Banesto";',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta4a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->select('AVG(edad) AS edad_media')->where('nomequipo="Banesto"'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta Nº4 con ORM",
            "enunciado"=>"La edad media de los del equipo Banesto.",
            "sql"=>'SELECT AVG(edad) AS edad FROM ciclista WHERE nomequipo="Banesto";',
        ]);
    }
    
    //Consulta Nº5
    
    //Utilizando el método DAO

    public function actionConsulta5(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `nomequipo`, AVG(edad) AS `edad_media` FROM `Ciclista` GROUP BY `nomequipo`) `c`;')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, AVG(edad) AS edad_media FROM ciclista GROUP BY nomequipo;',
            'totalCount'=>$numero,
            'pagination' => ['pageSize'=>0,]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta Nª5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>'SELECT nomequipo, AVG(edad) AS edad_media FROM ciclista GROUP BY nomequipo;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta5a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find() ->select('nomequipo, AVG(edad) AS edad_media') ->groupBy('nomequipo'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta Nº5 con ORM",
            "enunciado"=>"La edad media de los ciclistas por cada equipo.",
            "sql"=>'SELECT nomequipo, AVG(edad) AS edad_media FROM ciclista GROUP BY nomequipo;',
        ]);
    }
    
    //Consulta Nº6
    
    //Utilizando el método DAO

    public function actionConsulta6(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `nomequipo`, count(*) AS `total_ciclistas` FROM `Ciclista` GROUP BY `nomequipo`) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(*) AS total_ciclistas FROM ciclista GROUP BY nomequipo;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','total_ciclistas'],
            "titulo"=>"Consulta Nº6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo.",
            "sql"=>'SELECT nomequipo, COUNT(*) AS total_ciclistas FROM ciclista GROUP BY nomequipo;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta6a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find() ->select('nomequipo, count(*) AS total_ciclistas')->groupBy('nomequipo'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','total_ciclistas'],
            "titulo"=>"Consulta Nº6 con ORM",
            "enunciado"=>"El número de ciclistas por equipo.",
            "sql"=>'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8;',
        ]);
    }
    
        //Consulta Nº7
    
    //Utilizando el método DAO

    public function actionConsulta7(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM `Puerto`;')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS puertos FROM puerto;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta Nº7 con DAO",
            "enunciado"=>"El número total de puertos.",
            "sql"=>'SELECT COUNT(*) AS puertos FROM puerto;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta7a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Puerto::find() ->select('COUNT(*) AS puertos'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta Nº7 con ORM",
            "enunciado"=>"El número total de puertos.",
            "sql"=>'SELECT COUNT(*) AS puertos FROM puerto;',
        ]);
    }
    
            //Consulta Nº8
    
    //Utilizando el método DAO

    public function actionConsulta8(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM `Puerto` WHERE altura > 1500;')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS puertos FROM puerto WHERE altura > 1500;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta Nº8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500.",
            "sql"=>'SELECT COUNT(*) AS puertos FROM puerto WHERE altura > 1500;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta8a(){
            $dataProvider = new ActiveDataProvider([
                'query'=> Puerto::find() ->select('count(*) as puertos')->where('altura > 1500'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta Nº8 con ORM",
            "enunciado"=>"El número total de puertos mayores de 1500.",
            "sql"=>'SELECT COUNT(*) AS "Puertos" FROM puerto WHERE altura > 1500;',
        ]);
    }
    
            //Consulta Nº9
    
    //Utilizando el método DAO

    public function actionConsulta9(){
        
        $numero = Yii::$app->db
                ->createCommand('	
SELECT COUNT(*) FROM (SELECT `nomequipo`, count(*) AS `ciclistas` FROM `Ciclista` GROUP BY `nomequipo` HAVING count(*) > 4) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistas'],
            "titulo"=>"Consulta Nº9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas.",
            "sql"=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta9a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find() ->select('nomequipo, count(*) as ciclistas')->groupBy('nomequipo')->having('count(*) > 4'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistas'],
            "titulo"=>"Consulta Nº9 con ORM",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas.",
            "sql"=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4;',
        ]);
    }
    
            //Consulta Nº10
    
    //Utilizando el método DAO

    public function actionConsulta10(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `nomequipo`, count(*) AS `ciclistas` FROM `Ciclista` WHERE edad between 28 and 32 GROUP BY `nomequipo` HAVING count(*) > 4) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistas'],
            "titulo"=>"Consulta Nº10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.",
            "sql"=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4;', 
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta10a(){
            $dataProvider = new ActiveDataProvider([
                'query'=> Ciclista::find() ->select('nomequipo, count(*) as ciclistas')->where('edad between 28 and 32')->groupBy('nomequipo')->having('count(*) > 4'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistas'],
            "titulo"=>"Consulta Nº10 con ORM",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.",
            "sql"=>'SELECT nomequipo, COUNT(*) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4;',
        ]);
    }
    
            //Consulta Nº11
    
    //Utilizando el método DAO

    public function actionConsulta11(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `dorsal`, count(dorsal) AS `etapas` FROM `Etapa` GROUP BY `dorsal`) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas'],
            "titulo"=>"Consulta Nº11 con DAO",
            "enunciado"=>"Indicar el número de etapas que ha ganado cada uno de los ciclistas.",
            "sql"=>'SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta11a(){
             $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->select('dorsal, count(dorsal) as etapas')->groupBy('dorsal'),
                'pagination' =>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas'],
            "titulo"=>"Consulta Nº11 con ORM",
            "enunciado"=>"Indicar el número de etapas que ha ganado cada uno de los ciclistas.",
            "sql"=>'SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal;',
        ]);
    }
    
    
            //Consulta Nº12
    
    //Utilizando el método DAO

    public function actionConsulta12(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `dorsal`, count(dorsal) AS `etapas` FROM `Etapa` GROUP BY `dorsal` HAVING count(dorsal)>1) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1;',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas'],
            "titulo"=>"Consulta Nº12 con DAO",
            "enunciado"=>"Indicar el dorsal de los ciclistas que hayan ganado más de una etapa.",
            "sql"=>'SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta12a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Etapa::find() ->select('dorsal, count(dorsal) as etapas') ->groupBy('dorsal')->having('count(dorsal)>1'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas'],
            "titulo"=>"Consulta Nº12 con ORM",
            "enunciado"=>"Indicar el dorsal de los ciclistas que hayan ganado más de una etapa.",
            "sql"=>'SELECT dorsal, COUNT(dorsal) AS etapas FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1;',
        ]);
     }
     
    }